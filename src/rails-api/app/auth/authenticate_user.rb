class AuthenticateUser
  def initialize(login, password)
    @login = login
    @password = password
  end

  # Service entry point
  def call
    user = get_user_if_authenticate
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_reader :login, :password

  # verify user credentials
  def get_user_if_authenticate
    user = User.where(email: login).or(User.where(name: login)).first
    return user if user && user.authenticate(password)
    # raise Authentication error if credentials are invalid
    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end
end