require 'rails_helper'

RSpec.describe AuthenticateUser do
  # create test user
  let(:user) { create(:user) }
  # valid request subject
  subject(:valid_auth_obj_with_email) { described_class.new(user.email, user.password) }
  subject(:valid_auth_obj_with_name) { described_class.new(user.name, user.password) }
  # invalid request subject
  subject(:invalid_auth_obj) { described_class.new('foo', 'bar') }

  # Test suite for AuthenticateUser#call
  describe '#call' do
    # return token when valid request
    context 'when valid credentials' do

      context 'using email as login' do
        it 'returns an auth token' do
          token = valid_auth_obj_with_email.call
          expect(token).not_to be_nil
        end
      end

      context 'using name as login' do
        it 'returns an auth token' do
          token = valid_auth_obj_with_name.call
          expect(token).not_to be_nil
        end
      end
      
    end

    # raise Authentication Error when invalid request
    context 'when invalid credentials' do
      it 'raises an authentication error' do
        expect { invalid_auth_obj.call }
          .to raise_error(
            ExceptionHandler::AuthenticationError,
            /Invalid credentials/
          )
      end
    end
  end
end