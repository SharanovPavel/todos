Rails.application.routes.draw do
  
  post 'login', to: 'authentication#authenticate'
  
  post 'register', to: 'users#create'
  
  resources :todos do
    resources :items
  end
end